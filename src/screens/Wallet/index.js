import React, { useState, useEffect } from "react";
import { Switch } from "react-native";
import {
  Feather,
  MaterialCommunityIcons,
  FontAwesome,
  AntDesign,
} from "@expo/vector-icons";

import creditCard from "./../../images/credit-card.png";

import {
  Wrapper,
  Header,
  HeaderContainer,
  Title,
  Info,
  BalanceContainer,
  Value,
  Bold,
  EyeButton,
  Actions,
  Action,
  ActionLabel,
  UseBalance,
  UseBalanceTitle,
  PaymentMethods,
  PaymentMethodsTitle,
  Card,
  CardBody,
  CardButton,
  CardDetails,
  CardTitle,
  CardInfo,
  Img,
  CardButtonTitle,
  UseTicketButton,
  UseTicketLabel,
} from "./styles";

export default function Wallet() {
  const [visible, setVisible] = useState(false);
  const [useBalance, setUseBalance] = useState(true);

  function handleToggleVisible() {
    setVisible((prevState) => !prevState);
  }

  function handleToggleBalance() {
    setUseBalance((prevState) => !prevState);
  }

  return (
    <Wrapper>
      <Header
        colors={useBalance ? ["#52e78c", "#1ab563"] : ["#d3d3d3", "#868686"]}
      >
        <HeaderContainer>
          <Title>Saldo Picpay</Title>
          <BalanceContainer>
            <Value>
              R$ <Bold>{visible ? "0,00" : "----"}</Bold>
            </Value>
            <EyeButton onPress={handleToggleVisible}>
              <Feather
                size={28}
                color="#ffffff"
                name={visible ? "eye" : "eye-off"}
              />
            </EyeButton>
          </BalanceContainer>

          <Info>Seu saldo está rendendo 100% do CDI</Info>

          <Actions>
            <Action>
              <MaterialCommunityIcons name="cash" color="#ffffff" size={30} />
              <ActionLabel>Adicionar</ActionLabel>
            </Action>
            <Action>
              <FontAwesome name="bank" color="#ffffff" size={20} />
              <ActionLabel>Adicionar</ActionLabel>
            </Action>
          </Actions>
        </HeaderContainer>
      </Header>

      <UseBalance>
        <UseBalanceTitle>Usar saldo ao pagar</UseBalanceTitle>
        <Switch value={useBalance} onValueChange={handleToggleBalance} />
      </UseBalance>

      <PaymentMethods>
        <PaymentMethodsTitle>Forma de pagamento</PaymentMethodsTitle>
        <Card>
          <CardBody>
            <CardDetails>
              <CardTitle>Cadatre seu cartão de crédito</CardTitle>
              <CardInfo>
                Cadatre um cartão de crédito para poder fazer pagamentos mesmo
                quando não tiver saldo no seu PicPay.
              </CardInfo>
            </CardDetails>
            <Img source={creditCard} resizeMode="contain" />
          </CardBody>
          <CardButton>
            <AntDesign name="pluscircleo" size={30} color="#0db060" />
            <CardButtonTitle>Adicionar cartão de crédito</CardButtonTitle>
          </CardButton>
        </Card>

        <UseTicketButton>
          <MaterialCommunityIcons
            name="ticket-outline"
            size={20}
            color="#0db060"
          />
          <UseTicketLabel>Usar código promocional</UseTicketLabel>
        </UseTicketButton>
      </PaymentMethods>
    </Wrapper>
  );
}
