import React from "react";
import { Feather, MaterialCommunityIcons, AntDesign } from "@expo/vector-icons";

import {
  Container,
  Header,
  Title,
  Card,
  CardHeader,
  CardBody,
  UserName,
  Avatar,
  Description,
  Bold,
  CardFooter,
  Details,
  Value,
  Divider,
  Date,
  Actions,
  Option,
  OptionLabel,
} from "./styles";

import avatar from "./../../images/avatar.png";

export default function Activities() {
  return (
    <Container>
      <Header>
        <Title>Atividades</Title>
      </Header>

      <Card>
        <CardHeader>
          <Avatar source={avatar} />
          <Description>
            <Bold>Você</Bold> pagou à <Bold>@reactnative</Bold>
          </Description>
        </CardHeader>
        <CardBody>
          <UserName>Rodinei Teixeira</UserName>
        </CardBody>
        <CardFooter>
          <Details>
            <Value>R$ 50,00</Value>
            <Divider />

            <Feather name="lock" color="#ffffff" size={14} />
            <Date>há 1 mês</Date>
          </Details>

          <Actions>
            <Option>
              <MaterialCommunityIcons
                size={14}
                color="#ffffff"
                name="comment-outline"
              />
              <OptionLabel>0</OptionLabel>
            </Option>
            <Option>
              <AntDesign size={14} color="#ffffff" name="hearto" />
              <OptionLabel>0</OptionLabel>
            </Option>
          </Actions>
        </CardFooter>
      </Card>
    </Container>
  );
}
